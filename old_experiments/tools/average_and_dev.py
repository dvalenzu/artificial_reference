import sys
import numpy 

def average(s): return sum(s) * 1.0 / len(s)

def process_chunk(chunk):
    #print "a chunk:"
    values = []
    for record in chunk:
        values.append(float(record[1]))
        #print record[0] + "**" + record[1]
    np_arr = numpy.array(values)
    my_avg=numpy.mean(np_arr)
    my_dev=numpy.std(np_arr)
    print record[0] + " " + str(my_avg) + " " + str(my_dev)

def process_file(file_name):
  f = open(file_name);
  prev_key = "kk"
  for line in f:
    record = line.split();
    if(record[0] != prev_key):
        if(prev_key != "kk"):
            process_chunk(chunk)
        chunk = []
        chunk.append(record)
    else:
        chunk.append(record)
    prev_key = record[0]
  if(prev_key != "kk"):
    process_chunk(chunk)
  f.close();

n_args = len(sys.argv);
if(n_args != 2):
  print 'Number of arguments:', n_args, 'arguments, is incorrect:'
  sys.exit();
FILE_NAME=sys.argv[1];
process_file(FILE_NAME);
