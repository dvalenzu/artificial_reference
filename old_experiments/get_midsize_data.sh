#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

mkdir -p src_data
cd src_data

wget http://pizzachili.dcc.uchile.cl/repcorpus/real/cere.7z
wget http://pizzachili.dcc.uchile.cl/repcorpus/real/para.7z
wget http://pizzachili.dcc.uchile.cl/repcorpus/real/einstein.en.txt.7z

7z e para.7z
7z e cere.7z
7z e einstein.en.txt.7z


rm *.7z

cd ..

