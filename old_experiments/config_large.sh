#Dummy config file
CONFIG_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
#INPUT_FOLDER=/data02
INPUT_FOLDER=/home/dvalenzu/PHD/repos_corpus/repetitive_corpus/SAMP_1000Genomes_CHR21
N_ROUNDS=5
MULTI_K=( $(( 125*1024 )) $(( 250*1024 )) $(( 500*1024 )) )
MULTI_BLOCK_LEN=( 512 1024 2048 4096 )
MULTI_REF_LEN=( $(( 500*1024*1024 )) $(( 1000*1024*1024 )) $(( 2000*1024*1024 )) )
#MULTI_NAME=( "countries kernel.64G" )
MULTI_NAME=( "all.fasta" )

# Red reserved for LZ77
PALETTE[0]="0000CD"  # Medium Blue
PALETTE[1]="008000"  # Green
PALETTE[2]="9400D3"  # Dark Violet
PALETTE[3]="00FFFF"  # Cyan
PALETTE[4]="FFD700"  # Gold

LIGHTWEIGHT=0
