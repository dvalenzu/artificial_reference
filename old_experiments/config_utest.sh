#Dummy config file
CONFIG_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
INPUT_FOLDER=${CONFIG_DIR}/UTEST_src_data
N_ROUNDS=2
MULTI_K=( 150 300 600 )
MULTI_BLOCK_LEN=( 1000 2000 4000 )
MULTI_REF_LEN=( 600000 1200000 )
MULTI_NAME=( "dblp_repetitive_10M" )

# Red reserved for LZ77
PALETTE[0]="0000CD"  # Medium Blue
PALETTE[1]="008000"  # Green
PALETTE[2]="9400D3"  # Dark Violet
PALETTE[3]="00FFFF"  # Cyan
PALETTE[4]="FFD700"  # Gold

LIGHTWEIGHT=1
