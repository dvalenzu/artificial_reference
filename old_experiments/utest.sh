#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

cp config_utest.sh config.sh
source "./config.sh"

for NAME in ${MULTI_NAME[@]}
do
  #continue;  # to avoid the previous cleanup.
  rm -f ./compressed_data/${NAME}*
  rm -f ./artificial_refs/${NAME}*
  rm -f ./fix_ref_plotter/plotter_data/${NAME}*
  rm -f ./multivar_plotter/plotter_data/${NAME}*
  rm -f ./fix_ref_plotter/results/${NAME}*
  rm -f ./multivar_plotter/results/${NAME}*
done
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "Input folder: ${INPUT_FOLDER}..."
if [ ! -d ${INPUT_FOLDER} ]; then
  mkdir -p ${INPUT_FOLDER}
  cd ${INPUT_FOLDER} 
  wget http://pizzachili.dcc.uchile.cl/repcorpus/pseudo-real/dblp.xml.00001.2.7z
  p7zip -d dblp.xml.00001.2.7z
  head -c "$((10*1024*1024))"  dblp.xml.00001.2 > dblp_repetitive_10M
  rm -f dblp.xml.00001.2 
  rm -f dblp.xml.00001.2.7z 
  cd ${DIR}
fi
echo "Input is ready..."

cd multivar_plotter;

./make_refs_and_compress.sh
./make_plots.sh

cd ${DIR}

cd fix_ref_plotter;

./make_refs_and_compress.sh
./make_plots.sh

cd ${DIR}

echo "Unit Test Ok!"

