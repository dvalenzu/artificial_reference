#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail


TARGET_DIR="./data_to_go"
if [ -d ${TARGET_DIR} ]; then
  echo "Target dir: ${TARGET_DIR} already exists..."
fi

mkdir -p ${TARGET_DIR}
cp -r tools ${TARGET_DIR}
cp -r multivar_plotter ${TARGET_DIR}
cp -r fix_ref_plotter ${TARGET_DIR}
mkdir -p ${TARGET_DIR}/compressed_data
cp ./compressed_data/*.metadata.* ${TARGET_DIR}/compressed_data
cp config.sh ${TARGET_DIR}
