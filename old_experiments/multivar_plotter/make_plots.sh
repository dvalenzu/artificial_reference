#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

source "../config.sh"

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
COMPRESSED_DATA_DIR=${CONFIG_DIR}/compressed_data

for NAME in ${MULTI_NAME[@]}
do
  for K in ${MULTI_K[@]}
  do
    DATA_FILE_SIZES_ALL=${DIR}/plotter_data/${NAME}.k.${K}.size_in_bytes.all
    rm -f ${DATA_FILE_SIZES_ALL}

    DATA_FILE_FACTORS_ALL=${DIR}/plotter_data/${NAME}.k.${K}.factors.all
    rm -f ${DATA_FILE_FACTORS_ALL}

    for BLOCK_LEN in ${MULTI_BLOCK_LEN[@]}
    do
      for (( R_I=1; R_I<=${N_ROUNDS}; R_I++ ))
      do
        #echo "BLOCK_LEN: ${BLOCK_LEN}"
        COMPRESSED_FILE=${COMPRESSED_DATA_DIR}/${NAME}.rlz.k.${K}.len.${BLOCK_LEN}.round.${R_I}
        SIZE=`cat ${COMPRESSED_FILE}.metadata.size_in_bytes`
        echo "${BLOCK_LEN} ${SIZE}" >> ${DATA_FILE_SIZES_ALL}

        if [ "${LIGHTWEIGHT}" == "1" ]; then
          echo "Lightweight mode..."
        else
          SIZE_REAL=`stat --printf="%s" ${COMPRESSED_FILE}`
          echo "Are: ${SIZE} and ${SIZE_REAL} the same ?"
          if [ "${SIZE}" == "${SIZE_REAL}" ]; then
            echo "YES"
          else
            echo "ERROR: Size from metadata differ from real size from command stat"
            echo "Exiting..."
            exit
          fi
        fi
        FACTORS=`cat ${COMPRESSED_FILE}.metadata.nfactors`
        echo "${BLOCK_LEN} ${FACTORS}" >> ${DATA_FILE_FACTORS_ALL}
      done
    done
    DATA_FILE_SIZES_AVG=${DIR}/plotter_data/${NAME}.k.${K}.size_in_bytes.avg
    python ../tools/average_and_dev.py ${DATA_FILE_SIZES_ALL} > ${DATA_FILE_SIZES_AVG}

    DATA_FILE_FACTORS_AVG=${DIR}/plotter_data/${NAME}.k.${K}.nfactors.avg
    python ../tools/average_and_dev.py ${DATA_FILE_FACTORS_ALL} > ${DATA_FILE_FACTORS_AVG}
  done
done

for TO_PLOT in "size_in_bytes" "nfactors"
do
  for NAME in ${MULTI_NAME[@]}
  do
    CURRENT_PLOTTER=tmp_plotter_${TO_PLOT}_${NAME}.p
    cp generic_plotter.p ${CURRENT_PLOTTER}
    COUNTER=0
    for K in ${MULTI_K[@]}
    do
      COLOR=${PALETTE[${COUNTER}]}
      COUNTER=$((COUNTER+1))
      LINE1="\".\/plotter_data\/NAME.k.${K}.TO_PLOT_SUFFIX.avg\" using (\$1):((\$2+(${K}*\$1))\/DIVISOR) title \"K = ${K}, RLZ+Ref\" with linespoints lw 3 pt 0 ps 1 lt 1 lc rgbcolor \"\#${COLOR}\", \\\\"
      LINE2="\".\/plotter_data\/NAME.k.${K}.TO_PLOT_SUFFIX.avg\" using (\$1):((\$2+(${K}*\$1))\/DIVISOR):((\$3)\/DIVISOR)  with yerrorbars pt 0 lt 1 lc rgbcolor \"\#${COLOR}\" notitle, \\\\"
      LINE3="\".\/plotter_data\/NAME.k.${K}.TO_PLOT_SUFFIX.avg\" using (\$1):(((${K}*\$1))\/DIVISOR) with points lw 3 pt 5 ps 0.6 lt 1 lc rgbcolor \"\#${COLOR}\" notitle, \\\\"
      if [ "$TO_PLOT" = "size_in_bytes" ]; then
        sed -i "s/LINE_PLACEHOLDER/LINE_PLACEHOLDER\n${LINE3}/g" ${CURRENT_PLOTTER}
      fi
      sed -i "s/LINE_PLACEHOLDER/LINE_PLACEHOLDER\n${LINE2}/g" ${CURRENT_PLOTTER}
      sed -i "s/LINE_PLACEHOLDER/LINE_PLACEHOLDER\n${LINE1}/g" ${CURRENT_PLOTTER}
    done 
    sed -i '$s/...$//' ${CURRENT_PLOTTER}
    sed -i /LINE_PLACEHOLDER/d ${CURRENT_PLOTTER}

    DIVISOR="1"
    YUNIT="Number of factors"
    if [ "$TO_PLOT" = "size_in_bytes" ]; then
      DIVISOR="1048576"
      YUNIT="Output size (MBs)"
    fi

    N_LEN=${#MULTI_BLOCK_LEN[@]}
    N_LEN_MINUS=$((N_LEN-1))
    MAX_X=${MULTI_BLOCK_LEN[${N_LEN_MINUS}]}
    sed -i s/MAX_X/${MAX_X}/g ${CURRENT_PLOTTER}
    sed -i s/NAME/${NAME}/g ${CURRENT_PLOTTER}
    sed -i s/TO_PLOT_SUFFIX/${TO_PLOT}/g ${CURRENT_PLOTTER}
    sed -i s/DIVISOR/${DIVISOR}/g ${CURRENT_PLOTTER}
    sed -i s,COMPRESSED_DATA_DIR,${COMPRESSED_DATA_DIR},g ${CURRENT_PLOTTER}
    sed -i "s/YUNIT/${YUNIT}/g" ${CURRENT_PLOTTER}
    gnuplot < ${CURRENT_PLOTTER}
  done
done

rm tmp_*.p
mv *eps ./results
