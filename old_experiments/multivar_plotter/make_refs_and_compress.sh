#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

source "../config.sh"

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
COMPRESSED_DATA_DIR=${CONFIG_DIR}/compressed_data
REFS_DIR=${CONFIG_DIR}/artificial_refs


BUILD_DICT_BIN=${DIR}/../../do_dict/build_dictionary
CODER_BIN=${DIR}/../../coder/encode
LZ_BIN=${DIR}/../../LZscan/measure/count

for (( R_I=1; R_I<=${N_ROUNDS}; R_I++ ))
do
  for NAME in ${MULTI_NAME[@]}
  do
    INPUT=${INPUT_FOLDER}/${NAME}
    for K in ${MULTI_K[@]}
    do
      for BLOCK_LEN in ${MULTI_BLOCK_LEN[@]}
      do
        DICT=${REFS_DIR}/${NAME}.sample.k.${K}.len.${BLOCK_LEN}.round.${R_I}
        if [ ! -f ${DICT} ]; then
          echo "Making Dict"
          ${BUILD_DICT_BIN} ${INPUT} ${K} ${BLOCK_LEN} ${DICT} ${R_I} # &
        fi
      done
    done
    #wait 
    echo "****************"
    echo "COMPRESSION STAGE::::::"
    echo "****************"

    for K in ${MULTI_K[@]}
    do
      for BLOCK_LEN in ${MULTI_BLOCK_LEN[@]}
      do
        echo "MultiVar:"
        echo "Block Len: ${BLOCK_LEN}"
        echo "K: ${K}"
        DICT=${REFS_DIR}/${NAME}.sample.k.${K}.len.${BLOCK_LEN}.round.${R_I}
        COMPRESSED_FILE=${COMPRESSED_DATA_DIR}/${NAME}.rlz.k.${K}.len.${BLOCK_LEN}.round.${R_I}
        METADADA_FILE="${COMPRESSED_FILE}.metadata.size_in_bytes"
        LOG_FILE=log_multivar.k${K}.len.${BLOCK_LEN}
        if [ ! -f ${METADADA_FILE} ]; then
          echo "Compressing..."
          ${CODER_BIN} ${DICT} ${BLOCK_LEN} ${INPUT} ${COMPRESSED_FILE} > ${LOG_FILE}  &
        else
          echo "Reusing data file found..."
        fi
        if [ "${LIGHTWEIGHT}" == "1" ]; then
          echo "Lightweight mode...Cleaning up."
          rm -f ${COMPRESSED_FILE}
        fi 
      done
      wait
    done
    #wait 
  done
done

for NAME in ${MULTI_NAME[@]}
do
  INPUT=${INPUT_FOLDER}/${NAME}
  LZFILE_PREFIX=${COMPRESSED_DATA_DIR}/${NAME}.lz77
  LZ77_METADADA_FILE=${LZFILE_PREFIX}.metadata.size_in_bytes
  if [ ! -f ${LZ77_METADADA_FILE} ]; then
    ${LZ_BIN} ${INPUT} ${LZFILE_PREFIX}
  fi
done


