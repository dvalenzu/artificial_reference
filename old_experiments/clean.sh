#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

read -p "Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
  read -p "SURE? " -n 1 -r
  echo    # (optional) move to a new line
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    rm -f ./compressed_data/*
    rm -f ./artificial_refs/*
    rm -f ./fix_ref_plotter/plotter_data/*
    rm -f ./multivar_plotter/plotter_data/*
    rm -f ./fix_ref_plotter/results/*
    rm -f ./multivar_plotter/results/*
    rm -rf ./UTEST_src_data/
    echo "Data erased"
  fi
fi
