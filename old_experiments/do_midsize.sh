#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail


cp config_midsize.sh config.sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

cd multivar_plotter;

./make_refs_and_compress.sh
./make_plots.sh

cd ..

cd fix_ref_plotter;

./make_refs_and_compress.sh
./make_plots.sh

cd ..


