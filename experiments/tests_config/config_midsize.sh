#Dummy config file
CONFIG_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
WORKING_FOLDER=${CONFIG_DIR}/../working_folder
TOOLS_DIR=${CONFIG_DIR}/../..
INPUT_FOLDER="/home/dvalenzu/PHD/repos_corpus/repetitive_corpus"
N_ROUNDS=4
MULTI_K=( 150 300 600 1200 2400 )
MULTI_BLOCK_LEN=( 4000 8000 10000 20000 40000 80000 100000 )
MULTI_REF_LEN=( 12000000 24000000 48000000 )
INPUT_NAMES=( "cere para" )

# Red reserved for LZ77
PALETTE[0]="0000CD"  # Medium Blue
PALETTE[1]="008000"  # Green
PALETTE[2]="9400D3"  # Dark Violet
PALETTE[3]="00FFFF"  # Cyan
PALETTE[4]="FFD700"  # Gold

LIGHTWEIGHT=0
