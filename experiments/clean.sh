#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

read -p "Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
  read -p "SURE? " -n 1 -r
  echo    # (optional) move to a new line
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    rm -f ./working_folder/compressed_data/*
    rm -f ./working_folder/artificial_refs/*
    rm -f ./plotter_data/*
    rm -f ./results/*
    echo "Data erased"
  fi
fi
