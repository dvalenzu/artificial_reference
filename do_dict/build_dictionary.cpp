#include <stdlib.h>
#include <iostream>
#include <set>
#include <vector>
#include <algorithm>

using std::cout;
using std::cerr;
using std::endl;

bool any_sample_goes = false;

/*
   The following method was too slow for larger positions, needs to be reimplemented, and a hughe bitmap will also limit us.
   TODO(Daniel)
 As the  probability of overlap is indeed low, it may be quicler to do as follows:
    * Take all the positions in an array.
    * Sort it (we do it anyway to scan the disk sequentially later
    * Now, in linear time, check for overlaps and delete them.
    * Generate as many random positions as needed. Now I could do binary search in the sorted vector to find if there is an overlap or not.
      Now the logarithmic penalty is paid only in some inserts, a minority
*/

    /*
bool valid_sample(std::set<size_t> current_set, size_t new_samp, size_t length);
bool valid_sample(std::set<size_t> current_set, size_t new_samp, size_t length) {
  if (any_sample_goes) {
    return true;
  } 
  if (current_set.size() == 0) {
    return true;
  } else {
    auto succ_it = current_set.upper_bound(new_samp); 
    size_t successor = *succ_it;
    //cout << "new_samp:" << new_samp << endl;
    //cout << "new_samp + length :" << new_samp + length << endl;
    //cout << "successor: " << successor << endl;
    if (succ_it != current_set.end() && new_samp + length > successor) {
      //cout << "Will overlap -> not valid" << endl;
      return false;
    } else {
      //cout << "not overlap" << endl;
    }
    
    if (succ_it == current_set.begin()) {
      // there is no predecessor.
      return true;
    }
    succ_it--;
    size_t predecessor = *succ_it;
    //cout << "predecessor: " << predecessor << endl; 
    //cout << "predecessor + length : " << predecessor + length << endl;
    //cout << "new_samp: " << new_samp << endl;
    if (predecessor + length > new_samp) {
      //cout << "It is covered -> not valid" << endl;
      return false;
    } else {
      //cout << "Not covered." << endl;
    }
    return true;
  }
}
     */

int main(int argc, char ** argv) {
  if (argc != 6) {
    cerr << "Ussage:" << endl; 
    cerr << argv[0] << " input_filename n_samples sample_length output_filename seed" << endl;
    exit(33);
  }
  char * input_filename = argv[1];
  size_t n_samples = (size_t)atoi(argv[2]);
  size_t sample_size = (size_t)atoi(argv[3]);
  char * output_filename = argv[4];
  unsigned int seed = (unsigned int)atoi(argv[5]);
  srand(seed);
  
  size_t dict_size = n_samples*sample_size;
  if (dict_size > INT32_MAX) {
    size_t  limit = INT32_MAX;
    fprintf(stderr, "Dictionary will be too longe for divsufsort:\n");
    fprintf(stderr, "Dictionary Size: %lu \n", dict_size);
    fprintf(stderr, "Int 32 Max     : %lu \n", limit);

    exit(-1);
  } 

  cout << "Input file            : " << input_filename << endl;
  cout << "Number of Samples     : " << n_samples << endl;
  cout << "Sample Size           : " << sample_size << endl;
  cout << "Output file           : " << output_filename << endl;
  cout << "Output size(will be)  : " << dict_size << endl;
  cout << "------------------------------------" << endl;  
  
  FILE * infile = fopen(input_filename, "r");
  if (!infile) {
    cout << "Error opening file" << input_filename << "for reading." << endl;
    exit(EXIT_FAILURE);
  }
  fseek(infile, 0, SEEK_END);
  size_t text_length  = ftell(infile);
  fseek(infile, 0, SEEK_SET);
  
  cout << "Input length       : " << text_length << endl;
  cout << "------------------------------------" << endl;  
  if (dict_size*(size_t)3 > (size_t)2*text_length) {
    cerr << "Total sample size will be more than (2/3)LEN." << endl;
    cerr << "Maybe you want to use LZ-77 instead? " << endl;
    cerr << "not quitting..." << endl;
    //exit(-1);
  } 
  if (dict_size*(size_t)30  <  (size_t)100*text_length) {
    cout << "Sample size is smaller than (0.3)*Input" << endl;
    cout << "We will accept any sampling point." << endl;
    any_sample_goes = true;
  } else {
    cerr << "Sample with insurance of non-overlap is not implemented (efficiently enough) yet. " << endl;
    cerr << "quitting..." << endl;
    exit(-1);
  }
  std::vector<size_t> sampling_pos_set;

  size_t  rep = 0;
  size_t count = 0;
  while (rep < n_samples) {
    size_t pos = rand() %(text_length - sample_size);
    /*
    if (!valid_sample(sampling_pos_set, pos, sample_size)) {
      //cout << pos << " was already cover by set, next." << endl;
      continue;
    }
     */
    //cout << "Choosing sample from pos: " << pos << endl;
    //sampling_pos_set.insert(pos); // for std::set
    sampling_pos_set.push_back(pos);
    rep++;
    count++;
    /*
    if (count%1000 == 0) {
      cout << count << " attempts." << endl;
      cout << rep << " out of " << n_samples << " positions taken. " << endl;
    }
    */
  } 
  std::sort (sampling_pos_set.begin(), sampling_pos_set.end());      
  cout << "Sampling decided... now, sort positions, read and write starts." << endl;
  fflush(stdout);

  char * dictionary = new char[dict_size];
  rep = 0;
  for(auto pos : sampling_pos_set) {
    //cout << "Taking sample from pos: " << pos << endl;
    fseek(infile, pos, SEEK_SET);
    char * buff_ptr = dictionary + rep*sample_size;
    if (sample_size != fread(buff_ptr, sizeof(char), sample_size, infile)) {
      cout << stderr << "Error reading string from file" << endl;
      exit(1);
    }
    rep++;
  }    
  fclose(infile);

  FILE * outfile = fopen(output_filename, "w");
  if (!outfile) {
    cout << "Error opening file" << output_filename << "for writing" << endl;
    exit(EXIT_FAILURE);
  }
  if (dict_size != fwrite(dictionary, sizeof(char), dict_size, outfile)) {
    cout << "Error writing dictionary to file" << endl;
  } 
  fclose(outfile); 
  delete [] dictionary;
  cout << "Success." << endl;
}

