#!/usr/bin/env bash
set -o errexit
set -o nounset

seq 1000 > seq_1000.txt
valgrind --db-attach=yes --leak-check=full --show-leak-kinds=all  ./build_dictionary seq_1000.txt 5 100  kaka
#./build_dictionary seq_1000.txt 5 20  kaka

