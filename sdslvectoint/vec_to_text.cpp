#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <fstream>
#include <iostream>

#include <sdsl/vectors.hpp>

using namespace std;
using namespace sdsl;

int main(int argc, char **argv)
{
  if (argc != 3) {
    cerr << "Usaage : " << argv[0] << " input.sdsl output.plain" << endl;
    exit(EXIT_FAILURE);
  }
  
  string sdsl_filename = string(argv[1]);
  string output_filename = string(argv[2]);
  
  int_vector<> v;
  load_vector_from_file(v, sdsl_filename, 1);
  cout << "Vector read, n chars: " << v.size() << endl;
  char * text = new char[v.size()];
  for (size_t i = 0; i < v.size(); i++) {
    text[i] = (char)v[i];
  }
  string output_text(text, v.size());
  
  ofstream out_fs(output_filename);
  out_fs << output_text;
  out_fs.close();
  return EXIT_SUCCESS;
}

