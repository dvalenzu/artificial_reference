#!/usr/bin/env bash
set -o errexit
set -o nounset

cd ext;
./compile.sh;
cd ..;

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )


## OTHER EXT THAN ONLY NEED COMPUILE
TO_COMPILE="${DIR}/coder"
echo "**************************"
echo "Compiling ${TO_COMPILE}..."
cd ${TO_COMPILE}; make;
echo "${TO_COMPILE} ready..."
echo "**************************"
echo " "

TO_COMPILE="${DIR}/do_dict"

echo "**************************"
echo "Compiling ${TO_COMPILE}..."
cd ${TO_COMPILE}; make;
echo "${TO_COMPILE} ready..."
echo "**************************"
echo " "

TO_COMPILE="${DIR}/LZscan/measure"

echo "Compiling ${TO_COMPILE}..."
cd ${TO_COMPILE}; make;
echo "${TO_COMPILE} ready..."



