#!/usr/bin/env bash
set -o errexit
set -o nounset

# E2E validation od build dictionary, encoder and decoder.

MULTI_BLOCK_LEN=( 2000 4000 8000 10000 )
MULTI_K=( 100 300 500 )
N_ROUNDS=3

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

BUILD_DICT_BIN=${DIR}/do_dict/build_dictionary
CODER_BIN=${DIR}/coder/parallel_encode
DECODER_BIN=${DIR}/coder/parallel_decode


TEST_DIR=${DIR}/tmp_validation_test_data
#rm -rf ${TEST_DIR}
echo "Test folder: ${TEST_DIR}"
mkdir -p ${TEST_DIR}

cd ${TEST_DIR} 
wget http://pizzachili.dcc.uchile.cl/repcorpus/pseudo-real/dblp.xml.00001.2.7z
p7zip -d dblp.xml.00001.2.7z
head -c "$((20*1024*1024))"  dblp.xml.00001.2 > dblp_repetitive
cd ${DIR}

NAME="dblp_repetitive"
INPUT_FILE="${TEST_DIR}/${NAME}"

for (( R_I=1; R_I<=${N_ROUNDS}; R_I++ ))
do
  for K in ${MULTI_K[@]}
  do
    for BLOCK_LEN in ${MULTI_BLOCK_LEN[@]}
    do
      echo "Making Dictionary..."
      DICT=${TEST_DIR}/${NAME}.dict_sample.k.${K}.len.${BLOCK_LEN}.round.${R_I}
      ${BUILD_DICT_BIN} ${INPUT_FILE} ${K} ${BLOCK_LEN} ${DICT} ${R_I}
      
      echo "Compressing..."
      COMPRESSED_FILE=${TEST_DIR}/${NAME}.PARALLEL_rlz.k.${K}.len.${BLOCK_LEN}.round.${R_I}
      ${CODER_BIN} ${DICT} ${BLOCK_LEN} ${INPUT_FILE} ${COMPRESSED_FILE}
      
      echo "Decompressing..."
      DECOMPRESSED_FILE=${TEST_DIR}/${NAME}.decompressed.k.${K}.len.${BLOCK_LEN}.round.${R_I}
      ${DECODER_BIN}  ${DICT} ${COMPRESSED_FILE} > ${DECOMPRESSED_FILE}
      echo "Verifying (diff)..."
      diff ${DECOMPRESSED_FILE} ${INPUT_FILE}
      echo "Succesfully verified."
    done
  done
done

echo "Test succesfull"
