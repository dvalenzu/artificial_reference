#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

INPUT_FILE=$1

## yep, 5 G
#REF_SIZE=5000000000 

REF_SIZE=10000000

CODER_BIN=./parallel_encode
DECODER_BIN=./parallel_decode
BLOCK_LEN=${REF_SIZE}  ## encoder use size_t for this so it should work...

REF_FILE=tmp.reference
echo "Doing Prefix-ref ..."
head -c ${REF_SIZE} ${INPUT_FILE} > ${REF_FILE}
echo "Prefix-ref created with head..."
COMPRESSED_FILE=tmp.rlz_compressed
DECOMPRESSED_FILE=tmp.reconstructed
echo "Encoding..."
${CODER_BIN} ${REF_FILE} ${BLOCK_LEN} ${INPUT_FILE} ${COMPRESSED_FILE}
echo "Decoding..."
${DECODER_BIN}  ${REF_FILE} ${COMPRESSED_FILE} > ${DECOMPRESSED_FILE}
echo "Verifying (diff)..."
diff ${DECOMPRESSED_FILE} ${INPUT_FILE}

echo "Test succesfull"
