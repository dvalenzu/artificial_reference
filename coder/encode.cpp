#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <fstream>
#include <iostream>

#include "dictionary.h"
#include "common.h"

size_t parse_in_blocks(
    FILE * fp,
    Dictionary &d, 
    size_t pos_bits, 
    size_t len_bits, 
    size_t n, 
    Writer &w);
size_t parse_in_memory(
    uint8_t * t,
    Dictionary &d, 
    size_t pos_bits, 
    size_t len_bits, 
    size_t n, 
    Writer &w);
size_t parse_in_external_memory(
    std::ifstream &fs,
    Dictionary &d, 
    size_t pos_bits, 
    size_t len_bits, 
    size_t n, 
    Writer &w);

int fraction_i;
int fraction_total;

int main(int argc, char **argv)
{
  fraction_i = 0;
  fraction_total = 100;
  // There is no reason to turn this on, with the exception of potential debugging.
  // If set to true, the entire text to be compressed is load into memory, limiting the 
  // size of files that can be handled.
  bool in_memory = false;
  bool in_blocks = true;
  if (argc != 5) {
    fprintf(stderr, "usage: encode [DICTIONARY] [SAMPLE-SIZE] [INPUT] [OUTPUT]\n");
    exit(EXIT_FAILURE);
  }

  Dictionary d(argv[1], s2b(argv[2]));

  size_t pos_bits = clog2(d.n + 1);

  size_t len_bits = clog2(d.sample_size + 1);

  size_t n;
  printf("Encoder will use:\n");
  printf("%lu bits per pos\n", pos_bits);
  printf("%lu bits per len\n", len_bits);
  printf("%lu TOTAL BITS.\n", pos_bits + len_bits);
  FILE * file_out = fopen(argv[4],"w");
  Writer w(file_out);
  size_t n_factors;

  if (in_memory) {
    uint8_t *t = load_text(argv[3], &n);
    n_factors = parse_in_memory(t, d, pos_bits, len_bits, n, w); 
    delete[] t;
  } else if (in_blocks) {
    FILE * fp = open_file(argv[3], &n);
    n_factors = parse_in_blocks(fp, d, pos_bits, len_bits, n, w); 
    fclose(fp); 
  } else {
    std::ifstream fs;
    open_fs(argv[3], &fs, &n); 
    assert(fs.good());
    n_factors = parse_in_external_memory(fs, d, pos_bits, len_bits, n, w); 
  }

  char metadata_nfactors_filename[200];
  sprintf(metadata_nfactors_filename, "%s.metadata.nfactors", argv[4]);

  std::ofstream out_file_factors;
  out_file_factors.open (metadata_nfactors_filename);
  out_file_factors << n_factors << std::endl;
  out_file_factors.close();


  size_t total_bits = 80 + n_factors*(pos_bits+len_bits);
  size_t total_bytes = (total_bits+7)/8;

  char metadata_size_in_bytes_filename[200];
  sprintf(metadata_size_in_bytes_filename, "%s.metadata.size_in_bytes", argv[4]);

  std::ofstream out_file_size;
  out_file_size.open (metadata_size_in_bytes_filename);
  out_file_size << total_bytes << std::endl;
  out_file_size.close();
  return EXIT_SUCCESS;
}

// We don't mind if the blocks impact the compression, because it should be negleglible
size_t parse_in_blocks(
    FILE * fp,
    Dictionary &d, 
    size_t pos_bits, 
    size_t len_bits, 
    size_t text_len, 
    Writer &w) {
  w.write(pos_bits, 8);
  w.write(len_bits, 8);
  w.write(text_len, 64);

  size_t n_factors = 0;
  size_t block_len = 1000; 
  size_t block_offset = 0;
  while (block_offset < text_len) {
    size_t to_read = (block_offset + block_len < text_len) ? block_len : text_len - block_offset;
    uint8_t *buffer = new uint8_t[to_read + 1];
    if (to_read != fread(buffer, 1, to_read, fp)) {
      printf("ERR");
      exit(-1);
    }
    size_t i = 0;
    while (i < to_read) {
      Factor factor = d.at(buffer + i, to_read - i);
      w.write(factor.pos, pos_bits);
      w.write(factor.len, len_bits);
      i += (factor.len > 0) ? factor.len : 1;
      n_factors++;
    }

    block_offset += to_read;
  }
  printf("Parsing is ready\n");
  fflush(stdout);
  return n_factors;
}

size_t parse_in_memory(
    uint8_t * t,
    Dictionary &d, 
    size_t pos_bits, 
    size_t len_bits, 
    size_t n, 
    Writer &w) {

  fprintf(stderr, "Warning, loading the whole text in memory\n");
  w.write(pos_bits, 8);
  w.write(len_bits, 8);
  w.write(n, 64);

  size_t i = 0;
  size_t n_factors = 0;
  while (i < n) {
    Factor factor = d.at(t + i, n - i);
    w.write(factor.pos, pos_bits);
    w.write(factor.len, len_bits);
    i += (factor.len > 0) ? factor.len : 1;
    n_factors++;
    if (i > fraction_i*n/fraction_total) {
      float perc = (float)100*(float)i/(float)n;
      printf("Advance: %4.2f %%\n", perc);
      fflush(stdout);
      fraction_i++;
    }
  }
  printf("Parsing is ready\n");
  fflush(stdout);
  return n_factors;
}

size_t parse_in_external_memory(
    std::ifstream &fs,
    Dictionary &d, 
    size_t pos_bits, 
    size_t len_bits, 
    size_t n, 
    Writer &w) {

  w.write(pos_bits, 8);
  w.write(len_bits, 8);
  w.write(n, 64);

  size_t i = 0;
  size_t n_factors = 0;

  while (i < n) {
    Factor factor = d.at(fs, n - i);
    w.write(factor.pos, pos_bits);
    w.write(factor.len, len_bits);
    i += (factor.len > 0) ? factor.len : 1;
    n_factors++;
    if (i > fraction_i*n/fraction_total) {
      float perc = (float)100*(float)i/(float)n;
      printf("Advance: %4.2f %%\n", perc);
      fflush(stdout);
      fraction_i++;
    }
  }
  printf("Parsing is ready\n");
  fflush(stdout);
  return n_factors;
}
