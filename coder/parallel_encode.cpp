#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <vector>

#include <cstring>
#include <fstream>
#include <iostream>

#include "dictionary.h"
#include "common.h"
void partial_parse(size_t starting_pos, size_t length, char * file_name, Dictionary &d, std::vector<Factor> & ans) {
  std::ifstream fs;
  size_t dummy_n;
  open_fs(file_name, &fs, &dummy_n); 
  assert(fs.good());
  fs.seekg(starting_pos);
  assert(fs.good());
  size_t i = 0;
  while (i < length) {
    Factor factor = d.at(fs, length - i);
    ans.push_back(factor);
    i += (factor.len > 0) ? factor.len : 1;
  }
  return;
}

size_t parse_in_external_memory(
    char * file_name,
    Dictionary &d, 
    size_t pos_bits, 
    size_t len_bits, 
    size_t n_partitions,
    Writer &w);

int fraction_i;
int fraction_total;

int main(int argc, char **argv)
{
  fraction_i = 0;
  fraction_total = 100;
  if (argc != 5) {
    fprintf(stderr, "usage: encode [DICTIONARY] [SAMPLE-SIZE] [INPUT] [OUTPUT]\n");
    exit(EXIT_FAILURE);
  }
  
  // sample size can be set to MAX_INT to allow longer phrases and go to "normal" RLZ
  Dictionary d(argv[1], s2b(argv[2]));
  size_t n_partitions = 10;
  //size_t n_threads = 5;
  
  size_t pos_bits = clog2(d.n + 1);

  size_t len_bits = clog2(d.sample_size + 1);

  printf("Encoder will use:\n");
  printf("%lu bits per pos\n", pos_bits);
  printf("%lu bits per len\n", len_bits);
  printf("%lu TOTAL BITS.\n", pos_bits + len_bits);
  FILE * file_out = fopen(argv[4],"w");
  Writer w(file_out);
  size_t n_factors;
  
  char * file_name = argv[3];
    n_factors = parse_in_external_memory(file_name, d, pos_bits, len_bits, n_partitions, w); 
  
  char metadata_nfactors_filename[200];
  sprintf(metadata_nfactors_filename, "%s.metadata.nfactors", argv[4]);

  std::ofstream out_file_factors;
  out_file_factors.open (metadata_nfactors_filename);
  out_file_factors << n_factors << std::endl;
  out_file_factors.close();


  size_t total_bits = 48 + n_factors*(pos_bits+len_bits);
  size_t total_bytes = (total_bits+7)/8;
  
  char metadata_size_in_bytes_filename[200];
  sprintf(metadata_size_in_bytes_filename, "%s.metadata.size_in_bytes", argv[4]);

  std::ofstream out_file_size;
  out_file_size.open (metadata_size_in_bytes_filename);
  out_file_size << total_bytes << std::endl;
  out_file_size.close();
  return EXIT_SUCCESS;
}

size_t parse_in_external_memory(
    char * file_name,
    Dictionary &d, 
    size_t pos_bits, 
    size_t len_bits, 
    size_t n_partitions,
    Writer &w) {
    
  size_t n;
  std::ifstream fs;
  open_fs(file_name, &fs, &n); 
  assert(fs.good());
  
  w.write(pos_bits, 8);
  w.write(len_bits, 8);
  w.write(n, 64);
  
  std::vector<std::vector<Factor>> factor_lists(n_partitions);
#pragma omp parallel for
  for (size_t t = 0; t < n_partitions; t++) {
    size_t starting_pos = t*(n/n_partitions);
    size_t length = (t != n_partitions - 1) ? (n/n_partitions) : n - starting_pos;
    partial_parse(starting_pos, length, file_name, d, factor_lists[t]); 
  }
  std::cout << "Parsing ready, writing it." << std::endl; 
  size_t n_factors = 0;
  for (size_t t = 0; t < n_partitions; t++) {
    n_factors += factor_lists[t].size(); 
    
    for (size_t i = 0; i < factor_lists[t].size(); i++){
      Factor factor = factor_lists[t][i];
      w.write(factor.pos, pos_bits);
      w.write(factor.len, len_bits);
    }
  }
  std::cout << "n_factors: "<< n_factors << std::endl; 
  
  
  fflush(stdout);
  return n_factors;
}
