////////////////////////////////////////////////////////////////////////////////
// count.cpp
//   An simple tool demonstrating the use of LZscan. It counts the number of
//   phrases in the LZ77 factorization of a given file.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2013 Juha Karkkainen, Dominik Kempa and Simon J. Puglisi
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
////////////////////////////////////////////////////////////////////////////////

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <stdint.h>

#include <fstream>
#include <iostream>
#include <string>

#include "../algorithm/lzscan.h"

int clog2(int v) {
  assert(v > 0);
  return (size_t)ceil(log(v)/log(2));
}

int main(int argc, char **argv) {
  // Check arguments.
  if (argc != 3) {
    fprintf(stderr,"usage: %s <file> bs\n\n"
                   "Computes LZ-factorization of <file>\n", argv[0]);
    return EXIT_FAILURE;
  }

  // Parse block size.
  int bs = 500; 

  char * input_filename = argv[1];
  char * output_prefix = argv[2];

  // Read input text.
  unsigned char *X = NULL;
  FILE *f = fopen(input_filename, "r");
  if (!f) { perror(input_filename); exit(1); }
  fseek(f, 0, SEEK_END);
  int n = ftell(f);
  rewind(f);
  X = new unsigned char[n + 5];
  if (!X) {
    fprintf(stderr, "Error: cannot allocate t.\n");
    exit(1);
  }
  int r = fread(X, 1, n, f);
  if (r != n) {
    fprintf(stderr, "Error: fread error.\n");
    exit(1);
  }
  fclose(f);

  // Run the parsing algorithm.  
  clock_t parsing_start = clock();
  std::vector<std::pair<int, int> > answer;
  int nphrases = parse(X, n, bs << 20, &answer);
  fprintf(stderr, "Total parsing time: %.2Lf\n",
      ((long double)clock() - parsing_start) / CLOCKS_PER_SEC);
  fprintf(stderr, "Number of nphrases = %d\n", nphrases);
  int max_pos = 0;
  for (int i = 0; i < nphrases; i++) {
    max_pos = std::max(max_pos, answer[i].first);
  }
  int max_len = 0;
  for (int i = 0; i < nphrases; i++) {
    max_len = std::max(max_len, answer[i].second);
  }
  int bits_pos = clog2(max_pos);
  int bits_len = clog2(max_len);
  fprintf(stderr, "Max  Pos = %d\n", max_pos);
  fprintf(stderr, "Bits Pos = %d\n", bits_pos);
  fprintf(stderr, "Max Len = %d\n", max_len);
  fprintf(stderr, "Bits Len = %d\n", bits_len);
  
  int z = nphrases;
  int logn = clog2(n);
  fprintf(stderr, "(n, logn, z) = (%d, %d, %d)\n", n, logn, z);
  int64_t k = z*logn*logn; 
  long double l = std::sqrt((long double)n/(long double)k); 
  //fprintf(stderr, "(k,l) = (%d, %.2Lf)\n", k, l);
  std::cout << "k,l : " << k << ", " << l << std::endl;
  int total_size = nphrases*(bits_len+bits_pos);
  int size_in_bytes = total_size/8;
  fprintf(stderr, "**************************************\n");
  fprintf(stderr, "TOTAL SIZE IN BITS = %d\n", total_size);
  fprintf(stderr, "**************************************\n");
  fprintf(stdout, "TOTAL SIZE IN BYTES = \n%d\n", size_in_bytes);
  delete[] X;

  char metadata_nfactors_filename[200];
  sprintf(metadata_nfactors_filename, "%s.metadata.nfactors", output_prefix);
  std::ofstream nfactors_file;
  nfactors_file.open (metadata_nfactors_filename);
  nfactors_file << nphrases << std::endl;
  nfactors_file.close();
  
  char metadata_size_filename[200];
  sprintf(metadata_size_filename, "%s.metadata.size_in_bytes", output_prefix);
  std::ofstream size_file;
  size_file.open (metadata_size_filename);
  size_file << size_in_bytes << std::endl;
  size_file.close();

  return 0;
}
